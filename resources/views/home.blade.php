@extends('master')
@section('content')   
<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li class="active"><a href="#">Convert Page</a></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-6">
            
            <form class="form-horizontal" method="post" enctype="multipart/form-data" action="/upload">
                {{csrf_field()}}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Upload</strong> File</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <p>Please upload your CSV file.</p>
                    <!-- Message -->
                    @if(Session::has('message'))
                    <p >{{ Session::get('message') }}</p>
                    @endif
                </div>
                <div class="panel-body form-group-separated">
                                        
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">File</label>
                        <div class="col-md-6 col-xs-12">                                                                                                                                        
                            <input type="file" class="fileinput btn-primary" name="file" id="filename" title="Browse file"/>
                            <span class="help-block">Input type file</span>
                        </div>
                    </div>

                </div>
                <div class="panel-footer">
                    <button class="btn btn-primary pull-right">Upload</button>
                </div>
            </div>
            </form>
            
        </div>
        <div class="col-md-6">
            
            <form class="form-horizontal" method="post" enctype="multipart/form-data" action="/export">
                {{csrf_field()}}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Export</strong> Options</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <p>Please select export options.</p>
                    <!-- Message -->
                    @if(Session::has('message'))
                    <p >{{ Session::get('message') }}</p>
                    @endif
                </div>
                <div class="panel-body form-group-separated">

                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">File Extension</label>
                        <div class="col-md-5 col-xs-12">                                                                                            
                            <select class="form-control select" name="file_extension">
                                <option>XML</option>
                                <option>SQL</option>
                                <option>JSON</option>
                            </select>
                            <span class="help-block">Select File Extension</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Sort Data</label>
                        <div class="col-md-5 col-xs-12">  
                            <select class="form-control select" name="sort_data">
                                <option value="0">Select Please</option>
                                <option value="name">Name</option>
                                <option value="address">Address</option>
                                <option value="contact">Contact</option>
                                <option value="phone">Phone</option>
                                <option value="uri">Uri</option>
                                <option value="stars">Stars</option>
                            </select>
                        </div>
                        <div class="col-md-5 col-xs-12">
                            <select class="form-control select" name="sort_data_opt">
                                <option value="0">Select Please</option>
                                <option>ASC</option>
                                <option>DESC</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Group Data</label>
                        <div class="col-md-5 col-xs-12">                                                                                            
                            <select class="form-control select" name="group_data">
                                <option value="0">Select Please</option>
                                <option value="name">Name</option>
                                <option value="address">Address</option>
                                <option value="contact">Contact</option>
                                <option value="phone">Phone</option>
                                <option value="uri">Uri</option>
                                <option value="stars">Stars</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="panel-footer">
                    <button class="btn btn-primary pull-right">Export</button>
                </div>
            </div>
            </form>
            
        </div>
        <div class="col-md-12">
            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title">Data Table</h3>
                                    <ul class="panel-controls">
                                        <li><a href="/clear"><span class="fa fa-trash-o"></span></a></li>
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                    <div class="row" id="loadingSpinner" style="margin:20px 0 20px 0;">
                                        <img src="/img/ajax-loader.gif" style=""> Please wait...
                                    </div>
                                    <table class="table" id="hoteltable" style="display:none;">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Address</th>
                                                <th>Contact</th>
                                                <th>Phone</th>
                                                <th>Uri</th>
                                                <th>Stars</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($hotels as $key => $hotel)
                                            <tr>
                                                <td>{{$key}}</td>
                                                <td>{{$hotel->name}}</td>
                                                <td>{{$hotel->address}}</td>
                                                <td>{{$hotel->contact}}</td>
                                                <td>{{$hotel->phone}}</td>
                                                <td>{{$hotel->uri}}</td>
                                                <td>{{$hotel->stars}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
        </div>
    </div>                    
    
</div>
<!-- END PAGE CONTENT WRAPPER -->                                                
@endsection
@section('edit-js')
<script>
var table = $('#hoteltable').DataTable({
  "initComplete": function(settings, json) {$('#loadingSpinner').hide();$('#hoteltable').show();}
});
</script>
@endsection