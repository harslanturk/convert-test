<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotel;
use Session;
use DB;
use File;
use Response;
use Spatie\ArrayToXml\ArrayToXml;

class HomeController extends Controller
{
    public function index()
    {
    	$hotels = Hotel::all();
    	return view('home', ['hotels' => $hotels]);
    }

    public function upload(Request $request)
    {

		$file = $request->file('file');

		// File Details 
		$filename = $file->getClientOriginalName();
		$extension = $file->getClientOriginalExtension();
		$tempPath = $file->getRealPath();
		$fileSize = $file->getSize();
		$mimeType = $file->getMimeType();

		// Valid File Extensions
		$valid_extension = array("csv");

		// 2MB in Bytes
		$maxFileSize = 2097152; 

		// Check file extension
		if(in_array(strtolower($extension),$valid_extension)){

			  // File upload location
			  $location = 'uploads';

			  // Upload file
			  $file->move($location,$filename);

			  // Import CSV to Database
			  $filepath = public_path($location."/".$filename);

			  // Reading file
			  $file = fopen($filepath,"r");

			  $importData_arr = array();
			  $i = 0;

			  while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
			     $num = count($filedata );
			     
			     // Skip first row (Remove below comment if you want to skip the first row)
			     /*if($i == 0){
			        $i++;
			        continue; 
			     }*/
			     for ($c=0; $c < $num; $c++) {
			        $importData_arr[$i][] = $filedata [$c];
			     }
			     $i++;
			  }
			  fclose($file);
			  /*echo "<pre>";
			  print_r($importData_arr);
			  die();*/

			  // Insert to MySQL database
			  foreach($importData_arr as $key => $importData){

			  	if($key > 0)
			  	{
		  		 	if(preg_match('/[^\x20-\x7f]/', $importData[0]))
		  		 	{
		  		 		echo "1";

		  		 	}
		  		 	else if (filter_var($importData[5], FILTER_VALIDATE_URL) === FALSE)
		  		 	{
		  		 		echo "2";
		  		 	}
		  		 	else if($importData[2] < 0)
		  		 	{
		  		 		echo "3";
		  		 	}
		  		 	else
		  		 	{
					    $insertData = array(
					       "name"=>$importData[0],
					       "address"=>$importData[1],
					       "stars"=>$importData[2],
					       "contact"=>$importData[3],
					       "phone"=>$importData[4],
					       "uri"=>$importData[5]);
					    Hotel::create($insertData);
					}					
			  	}

			  }

		  	Session::flash('message','Import Successful.');
		}
		else
		{
		 Session::flash('message','Invalid File Extension.');
		}

		// Redirect to index
		return redirect()->back();
		}
	public function export(Request $request)
    {
    	$data = $request->all();
    	if($data['sort_data'] != 0)
    	{
    		if($data['sort_data_opt'] != 0)
    		{
    			$sort = "ASC";
    		}
    		else
    		{
    			$sort = $data['sort_data_opt'];
    		}

    		if($data['group_data'] != 0)
    		{
    			$hotels = Hotel::orderBy($data['sort_data'], $sort)->groupBy($data['group_data'])->get();
    		}
    		else
    		{
    			$hotels = Hotel::orderBy($data['sort_data'], $sort)->get();
    		}

    	}
    	else
    	{
    		if($data['group_data'] != 0)
    		{
    			$hotels = Hotel::groupBy($data['group_data'])->get();
    		}
    		else
    		{
    			$hotels = Hotel::all();
    		}

    	}
    	if($data['file_extension'] == "SQL")
    	{
    		\Spatie\DbDumper\Databases\MySql::create()
		    ->setDbName("convert_db")
		    ->setUserName("root")
		    ->setPassword("")
		    ->includeTables(['hotels'])
		    ->dumpToFile('dump.sql');
		    return Response::download(public_path('dump.sql'));
    	}
    	else if($data['file_extension'] == "JSON")
    	{
    		$hotels = DB::table('hotels')->get()->toJson();
    		$fileName = time() . '_datafile.json';
			File::put(public_path('/uploads/json/'.$fileName),$hotels);
			return Response::download(public_path('/uploads/json/'.$fileName));
    	}
    	else if($data['file_extension'] == "XML")
    	{
    		foreach($hotels as $key => $object)
			{
			    $arrays["hotel".$key] = $object->toArray();
			}
    		$result = ArrayToXml::convert($arrays);

    		$fileName = time() . '_datafile.xml';
			File::put(public_path('/uploads/xml/'.$fileName),$result);
			return Response::download(public_path('/uploads/xml/'.$fileName));
    	}
    	else{
	    	$xml = new XMLWriter();

		    $xml->openURI('documents/mailouts/example.xml');

		    $xml->startDocument('1.0');

		    $xml->startElement('sales');

		    foreach ($sales as $sale) {


		        $xml->startElement('sale');

		        $xml->writeElement('REF', $sale->SaleID);
		        $xml->writeElement('COUNTY', $sale->county);
		        $xml->writeElement('BOP1', $sale->SaleBop1);
		        $xml->writeElement('OFFICE', $sale->SaleOffice);


		        $xml->endElement();
		    }

		    $xml->endElement();
		    $xml->endDocument();

		    $xml->flush();    		
    	}
    }
    public function clear()
    {
    	Hotel::truncate();
    	return redirect()->back();
    }
}
